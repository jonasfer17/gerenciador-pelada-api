<?php

namespace App\Controller\Admin;

use App\Domain\Model\Pelada;
use App\Infrastructure\Enum\MensagemEnum;
use App\Infrastructure\Services\PeladaService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api")
 */
class PeladaController extends AbstractFOSRestController
{
    /**
     * @var PeladaService
     */
    private $peladaService;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param PeladaService $peladaService
     * @param SerializerInterface $serializerInterface
     */
    public function __construct(
        PeladaService $peladaService,
        SerializerInterface $serializerInterface
    ) {
        $this->peladaService = $peladaService;
        $this->serializer = $serializerInterface;
    }

    /**
     * @Route(  
     *     path="/pelada",
     *     methods={"GET"}
     * )
     */
    public function index(Request $request)
    {
        $result = $this->peladaService
            ->findAll(
                $request->get('limit', 10),
                $request->get('offset', 0)
            )
        ;

        return $this->json($result, Response::HTTP_OK);
    }

    /**
     * @Route(
     *     path="/pelada/{id}", 
     *     methods={"GET"}
     * )
     */
    public function show(int $id)
    {
        return $this->json($this->peladaService->find($id), Response::HTTP_OK);
    }

    /**
     * @Route(
     *     path="/pelada",
     *     methods={"POST"}
     * )
     */
    public function create(Request $request)
    {
        $pelada = $this->serializer
            ->deserialize(
                $request->getContent(), 
                Pelada::class, 
                'json'
            );

        $this->peladaService->create($pelada);
        
        return $this->json(MensagemEnum::REGISTRO_INSERIDO, Response::HTTP_CREATED);
    }

    /**
     * @Route(
     *     path="/pelada/{id}", 
     *     methods={"PUT"}
     * )
     */
    public function update(Request $request, int $id)
    {
        $dadosRequest = $this->serializer
            ->deserialize(
                $request->getContent(), 
                Pelada::class, 
                'json'
            );
            
        $this->peladaService->update($id, $dadosRequest);
        return $this->json(MensagemEnum::REGISTRO_ATUALIZADO, Response::HTTP_OK);
    }

    /**
     * @Route(
     *     path="/pelada/{id}", 
     *     methods={"DELETE"}
     * )
     */
    public function destroy(int $id)
    {
        $this->peladaService->remove($id);

        return $this->json(MensagemEnum::REGISTRO_REMOVIDO);
    }
}