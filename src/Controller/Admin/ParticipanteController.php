<?php

namespace App\Controller\Admin;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("api/", name="admin_")
 */
class ParticipanteController extends AbstractFOSRestController
{

    /**
     * @Route("participante/", name="home")
     */
    public function index()
    {
        return $this->json('ok', Response::HTTP_OK);
    }
}