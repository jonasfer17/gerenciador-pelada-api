<?php

namespace App\Infrastructure\Repository;

use App\Domain\Model\Pelada;
use App\Domain\Model\Responsavel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PeladaRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Pelada::class);
        $this->entityManager = $this->getEntityManager();
    }

    public function salvar(Pelada $pelada)
    {
        $this->entityManager->persist($pelada);
        $this->entityManager->flush();
    }

    public function findPeladas(
        Responsavel $responsavel,
        int $limit, 
        int $offset
    ) {
        return $this->createQueryBuilder('p')
            ->select([
                'p.id',
                'p.nome'
            ])
            ->where('p.responsavel = :responsavel')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->setParameter('responsavel', $responsavel)
            ->getQuery()
            ->getResult();
    }

    public function delete(Pelada $pelada)
    {
        $this->entityManager->remove($pelada);
        $this->entityManager->flush();
    }
}