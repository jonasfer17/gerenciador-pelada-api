<?php

namespace App\Infrastructure\Enum;

class MensagemEnum
{
    public const 
        MENSAGEM_URL_NAO_EXISTE = 'Nenhuma rota encontrada para essa requisição',
        PELADA_NAO_ENCONTRADA = 'Pelada não encontrada',
        REGISTRO_INSERIDO = 'Registro Inserido',
        REGISTRO_NAO_ENCONTRADO = 'Registro não encontrado',
        REGISTRO_ATUALIZADO = 'Registro atualizado',
        REGISTRO_REMOVIDO = 'Registro removido'
    ;
}