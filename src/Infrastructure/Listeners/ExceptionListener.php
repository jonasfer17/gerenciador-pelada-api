<?php

namespace App\Infrastructure\Listeners;

use App\Infrastructure\Enum\MensagemEnum;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $mensagem = $exception->getMessage();
        $response = new Response();
        $response->setStatusCode(Response::HTTP_BAD_REQUEST);

        switch($exception) {
            case $exception instanceof MethodNotAllowedHttpException:
                $response->setContent(MensagemEnum::MENSAGEM_URL_NAO_EXISTE);
                break;
            case $exception instanceof NotFoundHttpException:
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                $response->setContent($mensagem);
                break;
            default:
                dump($exception);die;
        }

        $event->setResponse($response);
    }
}