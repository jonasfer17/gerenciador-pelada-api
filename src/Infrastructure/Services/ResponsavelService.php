<?php

namespace App\Infrastructure\Services;

use App\Domain\Model\Responsavel;
use App\Domain\Model\Usuario;
use App\Infrastructure\Repository\ResponsavelRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ResponsavelService
{
    /**
     * @var ResponsavelRepository
     */
    private $responsavelRepository;

    /**
     * @var Usuario
     */
    private $usuarioLogado;

    /**
     * @param TokenStorageInterface $tokenStorageInterface
     * @param ResponsavelRepository $responsavelRepository
     */
    public function __construct(
        TokenStorageInterface $tokenStorageInterface, 
        ResponsavelRepository $responsavelRepository
    ) {
        $this->responsavelRepository = $responsavelRepository;
        $this->usuarioLogado = $tokenStorageInterface->getToken()->getUser();
    }

    /**
     * @return Responsavel
     */
    public function getResponsavel(): Responsavel
    {
        return $this->findResponsavel() ?? $this->createResponsavel(); 
    }

    /**
     * @return Responsavel|null
     */
    public function findResponsavel(): ?Responsavel
    {
        return$this->responsavelRepository
            ->findOneBy(['pessoa' => $this->usuarioLogado->getPessoa()]);
    } 

    /**
     * @return Responsavel
     */
    private function createResponsavel(): Responsavel
    {
        $responsavel = new Responsavel();
        $responsavel->setPessoa($this->usuarioLogado->getPessoa());
        $responsavel->createdAt();
        
        return $responsavel;
    }

}