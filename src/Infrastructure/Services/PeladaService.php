<?php

namespace App\Infrastructure\Services;

use App\Domain\Model\Pelada;
use App\Infrastructure\Enum\MensagemEnum;
use App\Infrastructure\Repository\PeladaRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PeladaService
{
    /**
     * @var ResponsavelService
     */
    private $responsavelService;

    /**
     * @var PeladaRepository
     */
    private $peladaRepository;

    public function __construct(
        ResponsavelService $responsavelService,
        PeladaRepository $peladaRepository
    ) {
        $this->responsavelService = $responsavelService;
        $this->peladaRepository = $peladaRepository;
    }
    
    /**
     * @param Pelada $pelada
     * @return void
     */
    public function create(Pelada $pelada): void
    {
        $pelada->setResponsavel($this->responsavelService->getResponsavel());
        $pelada->createdAt();

        $this->peladaRepository->salvar($pelada);
    }

    public function update(int $id, Pelada $dadosRequest)
    {
        /** @var Pelada */
        $pelada = $this->peladaRepository
            ->findOneBy(['id' => $id, 'responsavel' => $this->responsavelService->findResponsavel()]);

        if (!$pelada) {
            throw new NotFoundHttpException(MensagemEnum::REGISTRO_NAO_ENCONTRADO);
        }
        
        $pelada->setNome($dadosRequest->getNome() ?? $pelada->getNome());
        $pelada->updateAt();

        $this->peladaRepository->salvar($pelada);
    }

    /**
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function findAll(
        int $limit = 10,
        int $offset = 0
    ): array {
        return $this->peladaRepository
            ->findPeladas(
                $this->responsavelService->getResponsavel(), 
                $limit, 
                $offset
            );
    }

    public function find(int $id)
    {
        $pelada = $this->peladaRepository
            ->findOneBy(
                [
                    'id' => $id, 
                    'responsavel' => $this->responsavelService->findResponsavel()
                ]
            );
        
        if (!$pelada) {
            throw new NotFoundHttpException(MensagemEnum::REGISTRO_NAO_ENCONTRADO);
        }

        return $pelada;
    }

    /**
     * @param integer $id
     * @return void
     */
    public function remove(int $id): void
    {
        $pelada = $this->peladaRepository
            ->findOneBy(
                [
                    'id' => $id, 
                    'responsavel' => $this->responsavelService->findResponsavel()
                ]
            )
        ;

        if (!$pelada) {
            throw new NotFoundHttpException(MensagemEnum::REGISTRO_NAO_ENCONTRADO);
        }

        $this->peladaRepository->delete($pelada);
    }
}