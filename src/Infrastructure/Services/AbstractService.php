<?php

namespace App\Infrastructure\Services;

use App\Domain\Model\Usuario;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AbstractService
{
    /**
     * @var Usuario
     */
    private $usuarioLogado;

    public function __construct(TokenStorageInterface $tokenStorageInterface)
    {
        $this->usuarioLogado = $tokenStorageInterface->getToken()->getUser();
    }

    /**
     * @return Usuario
     */
    public function getUsuario(): Usuario
    {
        dump($this->usuarioLogado);die;
        return $this->usuarioLogado;
    }
}