<?php

namespace App\Domain\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Pelada
{
    use Timestamps;

    /**
     * @var int
     * 
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Responsavel
     * 
     * @ORM\ManyToOne(targetEntity="Responsavel", inversedBy="pelada", cascade={"persist"})
     */
    private $responsavel;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $nome;
    
    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNome(): string
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome(string $nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return Responsavel
     */ 
    public function getResponsavel(): Responsavel
    {
        return $this->responsavel;
    }

    /**
     * @param Responsavel $responsavel
     */ 
    public function setResponsavel(Responsavel $responsavel)
    {
        $this->responsavel = $responsavel;
    }
}