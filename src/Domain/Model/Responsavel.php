<?php

namespace App\Domain\Model;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Responsavel
{
    use Timestamps;

    /**
     * @var int
     * 
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Pessoa")
     */
    private $pessoa;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Pelada", mappedBy="responsavel")
     */
    private $pelada;
    
    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return Collection
     */ 
    public function getPelada(): Collection
    {
        return $this->pelada;
    }

    /**
     * @param Collection $pelada
     */ 
    public function setPelada(Collection $pelada): void
    {
        $this->pelada = $pelada;
    }

    /**
     * @return Pessoa
     */
    public function getPessoa(): Pessoa
    {
        return $this->pessoa;
    }

    /**
     * @param Pessoa $pessoa
     */ 
    public function setPessoa(Pessoa $pessoa)
    {
        $this->pessoa = $pessoa;
    }
}